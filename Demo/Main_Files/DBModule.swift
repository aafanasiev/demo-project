
import RealmSwift

let DB = DBModule.sharedInstance

class DBModule {
    static let sharedInstance = DBModule()
    
    fileprivate let realm = try! Realm()
    fileprivate let intervalBackgroundQueue = DispatchQueue(label: "background")
    fileprivate let mainQueue = DispatchQueue.main
    
}

extension DBModule {
    
    func getIntervals(onComplete: @escaping((Results<Interval>) -> ())) {
        
        intervalBackgroundQueue.async {
            let intervals = try! Realm().objects(Interval.self).sorted(byKeyPath: "beginTime", ascending: true)
            self.mainQueue.async {
                onComplete(intervals)
            }
        }
    }
}



// MARK: Write data
extension DBModule {
    
    func writeToDB(newInterval: Interval, onComplete: @escaping((Bool) -> ())) {
        
        _ = getIntervals { [unowned self] intervals in
            
            let reference = intervals.filter("beginTime == %@", newInterval.beginTime).map{ThreadSafeReference(to: $0)}.first
            self.intervalBackgroundQueue.async {
                autoreleasepool {
                    
                    let realm = self.realm
                    guard let resultReference = reference, let finalInterval = realm.resolve(resultReference) else{
                        self.mainQueue.async {
                            onComplete(false)
                        }
                        return
                    }
                    
                    try! self.realm.write {
                        self.realm.add(finalInterval)
                    }
                    self.mainQueue.async {
                        onComplete(true)
                    }
                }
            }
        }
    }
    
    // Method which save array of interval to DB
    func writeIntervalsArray(intervalArray: [Interval], onComplete: @escaping((Bool) -> ())) {
        
        _ = getIntervals { [unowned self] intervals in
            
            let filterArray = Set.init(intervalArray).subtracting(intervals)
            self.intervalBackgroundQueue.async {
                let realm = self.realm
                try! realm.write {
                    realm.add(filterArray)
                    self.mainQueue.async {
                        onComplete(true)
                    }
                }
            }
            
        }
        
    }
}

// MARK: Get data
extension DBModule {
    // Return array of days (start of the day) from all intervals
    func getArrayOfTrainingDays(dates: @escaping(([Date]) -> ())) {
        
        self.getIntervals { intervals in
            let allTrainingDates = (intervals.value(forKey: "beginTime") as! [Date]).map { NSCalendar.current.startOfDay(for: $0) }
            dates(Array(Set(allTrainingDates)).sorted())
        }
    }
    
    // Return all intervals of today
    func getTodayTrainingIntervals(results: @escaping(Results<Interval>) -> ()) {
        getIntervalsInDate(Date(), withStateFilter: .train, results: {results($0)})
    }
    
    // Return all intervals filtered by day and state
    func getIntervalsInDate(_ date: Date, withStateFilter state: State, results: @escaping (Results<Interval>) -> ()) {
        getIntervalsInDate(date, results: {results($0.filter("state == %@", state.rawValue))})
    }
}

// MARK: private functions
private extension DBModule {
    func getIntervalsInDate(_ date: Date, results: @escaping((Results<Interval>) -> ())) {
        let dayStart = NSCalendar.current.startOfDay(for: date)
        let dayEnd: Date = {
            var components = DateComponents()
            components.day = 1
            components.second = -1
            return NSCalendar.current.date(byAdding: components, to: dayStart)!
        }()
        
        self.getIntervals { intervals in
            results(intervals.filter("beginTime BETWEEN %@", [dayStart, dayEnd]))
        }
    }
}

