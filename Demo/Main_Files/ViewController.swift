
import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateDatabase()
    }
}

private extension ViewController {
    
    func updateDatabase() {
        let date = Date()
        for i in 1 ... 1000 {
            if let date = NSCalendar.current.date(byAdding: .second, value: -i, to: date) {
                let interval = Interval(beginTime: date, state: .train)
                interval.save(onSuccess: { (stats) in
                    if stats {
                        self.updateStatsView()
                    }
                })
            }
        }
    }
    
    func updateStatsView() {
        
        // Print all the intervals dates
        _ = DB.getArrayOfTrainingDays { dates in
            print(dates) // should be on the main thread
            print(Thread.isMainThread)
        }
        
        // Print first training interval begin time from today
        _ = DB.getTodayTrainingIntervals { results in
            if let todayTrainingInterval = results.first {
                print(todayTrainingInterval.beginTime) // should be on the main thread
                print(Thread.isMainThread)
            }
        }
        
        // Print first training and tracking intervals begin time from yesterday
        if let yesterday = NSCalendar.current.date(byAdding: .day, value: -1, to: Date()) {
            
            _ = DB.getIntervalsInDate(yesterday, withStateFilter: .train, results: { results in
                
                let trainIntervals = results.sorted(byKeyPath: "beginTime", ascending: true)
                let trackIntervals = results.sorted(byKeyPath: "beginTime", ascending: true)
                
                if let trainInterval = trainIntervals.first {
                    print(trainInterval.beginTime) // should be on the main thread
                    print(Thread.isMainThread)
                }
                if let trackInterval = trackIntervals.first {
                    print(trackInterval.beginTime) // should be on the main thread
                    print(Thread.isMainThread)
                }
            })
        }
    }
}

